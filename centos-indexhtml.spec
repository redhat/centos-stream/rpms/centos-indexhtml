Summary: Browser default start page for CentOS
Name: centos-indexhtml
Version: 10.1
Release: 1%{?dist}
URL: https://github.com/CentOS/centos-indexhtml
Source0: %{url}/archive/%{version}/%{name}-%{version}.tar.gz
License: Distributable
Group: Documentation
BuildArch: noarch
Provides: redhat-indexhtml = %{version}-%{release}

%description
The indexhtml package contains the welcome page shown by your Web browser,
which you'll see after you've successfully installed CentOS Stream.

%prep
%setup -q

%install
mkdir -p %{buildroot}%{_defaultdocdir}/HTML
cp -a * %{buildroot}%{_defaultdocdir}/HTML/

%files
%{_defaultdocdir}/HTML/*

%changelog
* Thu Jun 13 2024 Troy Dawson <tdawson@redhat.com> - 10.1-1
- Update to 10.1

* Tue Sep 26 2023 Troy Dawson <tdawson@redhat.com> 10.0-1
- Update for CentOS Stream 10

* Fri Nov 19 2021 Carl George <carl@george.computer> - 9.3-1
- Latest upstream
- Resolves: rhbz#2015275

* Wed Mar 3 2021 Michal Konecny <mkonecny@redhat.com> 9.0-0
- Update for CentOS 9 Stream

* Sun Apr 28 2019 Alain Reguera Delgado <alain.reguera@gmail.com> 8.0-0
- Roll in CentOS artwork based on CentOS Artistic Motif Sketch 4

* Sun Jun 29 2014 Johnny Hughes <johnny@centos.org> 7-9.el7.centos
- Add en-US directory

* Fri May 16 2014 Johnny Hughes <johnny@centos.org> 7-8.el7.centos
- Roll in CentOS Branding
